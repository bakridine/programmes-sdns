# Santé publique France

Santé publique France est l’agence nationale de santé publique. Créée en mai 2016 par ordonnance et décret, c’est un établissement public administratif sous tutelle du ministère chargé de la Santé. Sa mission : améliorer et protéger la santé des populations.

## Mainteneurs du dossier:

- Laurence Mandereau-Bruno: [laurence.mandereau-bruno@santepubliquefrance.fr](laurence.mandereau-bruno@santepubliquefrance.fr)
- Elodie Moutengou: <elodie.moutengou@santepubliquefrance.fr>