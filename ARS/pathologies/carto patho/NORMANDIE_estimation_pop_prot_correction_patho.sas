option mprint symbolgen;
/********************************************************************/
/****Estimation  de la population prot�g�e RG+SLM qui va nous servir 
/*****de d�nominateur pour la carto des patho
  */
/*****************************************************************/

/***Entr�e des param�tres ***/
* Liste du ou des d�partement entre cote avec virgule si plusieurs;
%LET DEPT ='014','027','050','061','076';
%let ANNEE=2015;


* FIN des param�tres en entr�e;


/********************************************************************************/
/** calcul  des benef consommants  et non consommant sur IR_BEN_R      */
/* on ne retient que les R�gimes et SLM qui reemontent l'info de Non consommants */
/*********************************************************************************/

* Note En infra d�partementales on �limine les militaires '08A';

/*** SLM 
la mutuelle g�n�rale LMG 01MXXX619,
 la mutuelle g�n�rale de la police MGP 01MXXX537,
 MFP service MFPS 01MXXX599,
 la mutuelle nationale des hospitaliers MNH 01MXXX619,
 Harmonie fonction publique HFP 01MXXX516,
 la mutuelle des �tudiants LMDE (depuis janvier 2017) 01MXXX601,
 la caisse d�assurance maladie des industries �lectriques et gazi�res CAMIEG 01MXXX603,
 la mutuelle du minist�re de l�int�rieur Int�riale 01MXXX604,

***/
PROC SQL;
   CREATE TABLE WORK.POP_CONSO_NONCONSO AS 
   SELECT distinct 
	    t1.BEN_SEX_COD,
		case 
		when (&annee-INPUT(t1.BEN_NAI_ANN,4.))<= 9	then "[0-9]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=10 and((&annee-INPUT(t1.BEN_NAI_ANN,4.))<20)) then "[10-19]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=20 and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<30)) then "[20-29]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=30 and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<40)) then "[30-39]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=40 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<50)) then "[40-49]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=50 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<60)) then "[50-59]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=60 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<70)) then "[60-69]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=70 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<80)) then "[70-79]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=80 and  ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<90)) then "[80-89]"
		when ((&annee-INPUT(t1.BEN_NAI_ANN,4.))>=90  and ((&annee-INPUT(t1.BEN_NAI_ANN,4.))<150))  then "[90+]"
		end as class_age,
		t1.BEN_RES_DPT, 
          t1.BEN_RES_COM, 
            (COUNT(DISTINCT(case when t1.BEN_TOP_CNS=1 then t1.BEN_IDT_ANO end))) AS NB_CONSO_REF, /* On se sert du  TOP Conso */
			(COUNT(DISTINCT(t1.BEN_IDT_ANO))) AS NB_TOT_REF
      FROM ORAVUE.IR_BEN_R t1
      WHERE t1.BEN_RES_DPT in(&DEPT)
	  AND (SUBSTR(t1.ORG_AFF_BEN,1,3) in('01C'/*,'06A','07A','10A','90A'*/) 
	  				OR (SUBSTR(t1.ORG_AFF_BEN,1,3)='01M' AND SUBSTR(t1.ORG_AFF_BEN,7,3)in('619','537','599','619','516','601','603','604'))
					) 
	 AND t1.BEN_NAI_ANN < '2016' /* Elimination des naissances 2016*/
	 AND (t1.BEN_DCD_AME > '201512' OR t1.BEN_DCD_AME = '160001') /* Elimination  d�c�des ant�rieurs � 2016 en gardant les vivants!!*/
	AND t1.BEN_NAI_ANN not in ('160001')
GROUP BY /*1,2,3,4*/t1.BEN_RES_DPT,
               t1.BEN_RES_COM,
			   t1.BEN_SEX_COD,
class_age		;
QUIT;

* Correction des code communes erron�s;
* m�me manip que pr�c�demment;
PROC SQL;
   CREATE TABLE SASDATA1.POP_REF_Carto AS 
   SELECT  distinct
                 case when t2.CODE_INSEE is not null then t2.CODE_INSEE else substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM end as cod_comm , 
				  t2.LIBELLE_DE_LA_COMMUNE, 
				  class_age,
				  t1.BEN_SEX_COD,
                  sum(t1.NB_CONSO_REF) AS NB_CONSO_REF , 
                  sum(t1.NB_TOT_REF) AS NB_TOT_REF   
      FROM WORK.POP_CONSO_NONCONSO t1
           LEFT JOIN PRFEXT.T_FIN_GEO_LOC_FRANCE t2 ON (substr(t1.BEN_RES_DPT,2,2)||t1.BEN_RES_COM = t2.CODE_JOINTURE)
	group by case when t2.CODE_INSEE is not null then t2.CODE_INSEE else t1.BEN_RES_DPT||t1.BEN_RES_COM end ,
    t2.LIBELLE_DE_LA_COMMUNE, t1.ben_sex_cod, class_age;
QUIT;

