

/**************************************************************************************************/
/*********************************ALZHEIMER *******************************************************/
/* LEROY (ARS NORM.) & BROCCA (ARS CVL)															*/
/**************************************************************************************************/

/* on utilise les codes CIM 10 et non le numero d'ALD qui n'est pas tjs bien renseigne et pas forcement stable dans le temps: une table de correspondance IR_CIM_V existe pour 
identifier la correspondance ALD et Code CIM10 */ 
/* IR_IMB_R est une table france enti�re */ 


/* parametre en entree*/ 
%let ddd=20180101;
%let ddf=20181231;
%let dpt='014','027','050','061','076';




/** rcupereation de tous les b�n�f avec une ALD  active en 2017 dans le IR_IMB_R;*/

proc sql;
drop table ald_alzeihmer;
%connectora;
create table ald_alzeihmer as 
select * from connection to oracle (select 
BEN_NIR_PSA,
BEN_RNG_GEM,
IMB_ALD_NUM,
IMB_ALD_DTD,
IMB_ALD_DTF,
MED_MTF_COD
from IR_IMB_R 
where IMB_ALD_DTD <=to_date(&ddd.,'YYYYMMDD')
and (IMB_ALD_DTF>=to_date(&ddf.,'YYYYMMDD') or IMB_ALD_DTF=to_date ('16000101','YYYYMMDD'))
and substr(MED_MTF_COD,1,3) in ('F00','F01','F02','F03','G30')		
);
disconnect from oracle;
quit;


/** On cr�e une table avec une ligne par patient ALD **/
/** On tope si le patient a une ALD Alzheimer active et/ou Autre d�mences **/
/** creation d'un boolean pour faire un top */ 
proc sql;
create table ald_alz as select distinct
ben_nir_psa, ben_rng_gem ,max (case when substr(MED_MTF_COD,1,3) in ('F00','G30') then 1 else 0 end) as top_alz_ald,
max (case when substr(MED_MTF_COD,1,3) not in ('F00','G30') then 1 else 0 end) as top_demence_ald from ald_alzeihmer
group by ben_nir_psa, ben_rng_gem ;
quit;


/* on met la table dans ORAUSER pour faire jointure avec IR_BEN_R (recuperation du Departement) */ 


proc sql;
create table orauser.ald_alz as select * from work.ald_alz;
quit;



proc sql;
drop table ald_alzdept;
%connectora;
create table ald_alzdept as 
select * from connection to oracle (select 
t2.BEN_RES_DPT,
t1.BEN_NIR_PSA,
t1.BEN_RNG_GEM,
top_alz_ald,
top_demence_ald
from ald_alz t1 left join IR_BEN_R t2 on (T1.BEN_NIR_PSA=t2.BEN_NIR_PSA 
and t1.BEN_RNG_GEM=t2.BEN_RNG_GEM) 
);
disconnect from oracle;
quit;


proc sql;
create table final_ALD as select
ben_res_dpt,
sum(top_alz_ald) as nb_alz_ald,
sum(top_demence_ald) as nb_demence
from ald_alzdept
group by 1;
quit;
 

proc sQL;
create table restitution_dpt as select * from final_ald 
where ben_res_dpt in (&dpt.);
quit;





